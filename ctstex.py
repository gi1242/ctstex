#! /usr/bin/python3
# Continuously watch a file and call latexmk when needed.

import pyinotify as pyi
import os, sys, signal, time, argparse, threading

class EventHandler( pyi.ProcessEvent ):
    def process_IN_CLOSE_WRITE( self, event ):
        #debug( 'current_thread={}, get_ident={}, active_count={}'.format(
        #    threading.current_thread, threading.get_ident(),
        #    threading.active_count() ) )
        f = p.relpath( event.pathname )
        if f in deps or \
                ( args.bib and f.endswith('.bib') ):
            debug( '{} modified, rerunning.'.format(f) )
            timer.set()
        else:
            debug( '{} modified, skipped.'.format(f) )

def log( *fargs, start='\033[32m' ):
    print( start, time.strftime('%H:%M:%S: '), sep='', end='',
            file=sys.stderr )
    print( *fargs, end='', file=sys.stderr )
    print( '\033[m', file=sys.stderr )

def debug( *fargs ):
    if args.debug:
        log( *fargs, start='\033[31m')

def sigint_handler( signal, frame ):
    log( 'Got SIGINT.' )
    sys.exit()

def sighup_handler( signal, frame ):
    global got_sighup

    log( 'Got SIGHUP. Rerunning', cmd )
    got_sighup = True
    timer.set()

def run_cmd():
    global got_sighup

    c = sighup_cmd if got_sighup else cmd
    got_sighup = False

    log( 'Running ' + c )
    os.system( c )
    return get_deps()

def get_deps():
    fls = p.splitext( filename )[0] + '.fls'
    newdeps = set()
    try:
        f = open( fls, 'r' )
        for line in f:
            if line.startswith( 'INPUT ' ) and line[6] != '/':
                d = line[6:-1]
                if p.splitext(d)[1] not in exclude:
                    newdeps.add( p.relpath( p.realpath(d) ))
    except IOError as e:
        debug( 'Error opening {}: {}'.format( fls, e.strerror ) )
    newdeps.add( filename )
    debug( 'Dependencies:', newdeps )
    return newdeps

# Process options
parser = argparse.ArgumentParser(
        description='Continuously watch a file and call latexmk when needed.')

parser.add_argument( '-b', '--bib',
        action='store_false',
        help="Don't rerun latexmk when .bib files change" )
parser.add_argument( '-c', '--cmd',
        default='latexmk {}', type=str, dest='cmd',
        help='Command to execute (default: %(default)s)' )
parser.add_argument( '-C', '--sighup-cmd',
        default='latexmk -g {}', type=str, dest='sighup_cmd',
        help='Command to execute on SIGHUP (default: %(default)s)' )
parser.add_argument( '-d', '--debug',
        action='store_true',
        help='Debug info' )
parser.add_argument( '-I', '--dont-ignore-new',
        action='store_false', dest='ignore_new',
        help="Don't ignore file modifications made while command runs" )
parser.add_argument( '-p', '--prune',
        default='.git,.svn,tmp', type=str,
        help='Directories to prune from watch (default: %(default)s)' )
parser.add_argument( '-R', '--no-recurse',
        dest='recurse', action='store_false',
        help="Don't recurse into subdirectories" )
parser.add_argument( '-s', '--silence',
        type=int, default=200,
        help='duration (ms) of inactivity to wait for \
                before running cmd (default: %(default)d)' )
parser.add_argument( '-t', '--timeout',
        type=int, default=60,
        help='Idle timeout in minutes (default: %(default)d). Use 0 to disable' )
parser.add_argument( '-x', '--exclude',
        default='bbl,aux,out', type=str,
        help='Filetypes to exclude from dependencies (default: %(default)s)')
parser.add_argument( 'filename',
        help='Filename to compile' )
args = parser.parse_args()

# Setup command
p = os.path
(dirname, filename) = p.split( args.filename )
if dirname: os.chdir( dirname )
cmd = args.cmd.format( filename )
sighup_cmd = args.sighup_cmd.format( filename )
exclude = set( '.' + ext for ext in args.exclude.split(',') )
deps = set()

# Set up watches
wm = pyi.WatchManager()
mask = pyi.IN_CLOSE_WRITE
wdd = wm.add_watch( '.', mask, rec=False)

# Recurse into non-pruned subdirectories
if args.recurse:
    prune = args.prune.split(',')
    for (root, dirs, files) in os.walk( '.', topdown=True ):
        dirs[:] = [d for d in dirs if d not in prune]
        for d in dirs:
            wdd.update( wm.add_watch(
                p.relpath( p.join( root, d )), mask, rec=False ) )
    debug( 'Recursively watching:', set( wdd.keys() ) )

handler = EventHandler()

# Trap signals
signal.signal( signal.SIGINT, sigint_handler )
signal.signal( signal.SIGHUP, sighup_handler )
got_sighup = False

# Wait for events
notifier = pyi.ThreadedNotifier( wm, handler)
notifier.setDaemon( True )
timer = threading.Event()
notifier.start()

while True:
    deps = run_cmd()

    if args.ignore_new:
        debug( 'Flushing changes' )
        timer.clear()
    debug( 'Waiting for changes' )
    if timer.wait( args.timeout*60 ):
        while args.silence:
            timer.clear()
            debug( 'Waiting for {}s of inactivity'.format( args.silence/1000 ) )
            if not timer.wait( args.silence / 1000 ):
                break
    else:
        break

log( 'Exiting due to inactivity' )
