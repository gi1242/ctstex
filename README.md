# Continuously watch a file and call latexmk when needed

This script watches a directory using `inotify`, and re-runs `latexmk`
whenever it looks like the file (or any of its dependencies) have changed.
This functionality is provided by `latexmk` itself via the `-pvc` option.
However, `latexmk` continuously polls (without using `inotify`) and was a
higher drain on my laptop battery than I would like. So I wrote a simple
script to poll efficiently.

Few points to be aware of:

* The algorithm is **simple and stupid**, and so `latexmk` might be run an
  extra time when files are modified.

* It gets input dependencies from the `.fls` file generated by `latexmk`. This
  unfortunately doesn't include `.bib` files, so we re-run `latexmk` when
  **any** `.bib` in the current directory changes.

* It only watches for `CLOSE_WRITE` events; so if your editor modifies a file
  in place without closing it, then the file won't be recompiled.

* It's still a lot more efficient than `latexmk -pvc`.

Use `ctstex -h` for options and parameters.
